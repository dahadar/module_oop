#include <iostream>
#include <cstdlib>
using namespace std;


class Array
{
private:
    float * ptr;
    int size;
    int numElement;

public:
    Array();
    void show(int );
    void reset_element(float ,int );
    void insertValueAtEnd(float );
    void print();
};

Array::Array()
{
    size = 3;
    numElement = 0;
    ptr = new float[size];
    for (int i = 0; i < size; ++i)
    {
        ptr[i] = 0.1 * (rand() % 101);
    }
}

void Array::insertValueAtEnd(float insert)
{
    if(numElement == size){size++;}
    ptr[size-1] = insert;
    numElement++;
}

void Array::reset_element(float num, int i){
    ptr[i] = num;
}

void Array::show(int num){
    cout << ptr[num]<< ' ';
}

void Array::print(){
    for (int i = 0; i < size; i++)
        cout << ptr[i]<< ' ';
}
int function() {
    cout<<"The program generates a set of 10 random fractional numbers from 0 to 1, calculating their sum"<<'\n'<<
        "and there will be a minimum and a maximum number among them."<<endl;
    float random_array[10] = {};
    float sum = 0;
    float min = 1;
    float max = 0;
    for(int i = 0; i < 10; i++)
    {
        random_array[i] = 0.01 * (rand() % 101);
        if(max<random_array[i]){max = random_array[i];}
        if(min > random_array[i]){min = random_array[i];}
        sum += random_array[i];
        cout << random_array[i] << endl;
    }
    cout<< "sum :"<<sum<< endl;
    cout<< "min :"<<min<< endl;
    cout<< "max :"<<max<< endl;
    //cout << "Hello, World!" << endl;
    return 0;
}
int main(){
    // exercise b)
    Array array;
    array.insertValueAtEnd(3.2);
    cout<<endl;
    array.print();
    cout<<endl;
    array.show(1);
    array.reset_element(3.5,0);
    cout<<endl;
    array.print();
    cout<<endl;
    //exercise a)
    function();
}
